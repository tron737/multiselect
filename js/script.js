$(document).ready(function () {
    $("#bx1, #bx2, #bx3, #bx4, #bx5").niceScroll({
        cursorborder: "",
        cursorcolor: "#00cde3",
        boxzoom: false
    });

    $('#multiselect-block').customMultiSelect({
        nameInput: 'items[]',
        data: {
            1: ['a'],
            2: ['b'],
            3: ['c'],
            4: ['d'],
        },
        containerForChosedElements: $('.selected-items'),
    });
});