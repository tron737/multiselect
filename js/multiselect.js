(function ($) {
    $.fn.customMultiSelect = function (options) {

        var methods = {
            /**
             * Блок в котором будет отрисован мультиселект
             */
            container: '',
            /**
             * Имя input которые будут сгенерированы в выпадающем списке
             */
            nameInput: '',
            /**
             * Тип input
             */
            typeInput: 'checkbox',
            /**
             * Входные данные - объект {NAME: ['VALUE']}
             */
            data: {},
            /**
             * Селектор для поиска блока с сгенерированными элементами
             */
            selectorForBlockData: 'js-block-for-data',
            /**
             * Селектор для поиска блока для показа/скрытия
             */
            blockClickForOpen: 'js-block-opened',
            /**
             * Селектор для поиска блока в котором лежит список с сгенерированными элементами
             */
            blockForList: 'js-block-list',
            /**
             * Объект в который будут выведены выбранные элементы из выпадающего списка
             */
            containerForChosedElements: {},
            /**
             * Флаг необходимости вывода элемента выбора всех записей сразу
             */
            needSelectAll: true,
            /**
             * Имя элемента выбора всех элементов
             */
            selectAllText: 'Все',
            /**
             * Селектор для поиска элемента, который отвечает за выбор всех элементов сразу
             */
            selectAllObject: 'js-select-all',
            /**
             * Селектор для поиска элемента в котором выведено имя по умолчанию
             */
            selectForDefaultName: 'js-default-name',
            /**
             * Текст, который будет выведен в видимом блоке по умолчанию
             */
            textForDefaultName: 'Выбрано:',
            /**
             * Селектор поиска блока для вывода количества выбранных элементов или имени выбранного элемента
             */
            selectForCustomName: 'js-custom-name',
            /**
             * Количество допустимых символов в блоке selectForCustomName
             */
            maxTextlength: 22,
            /**
             * Флаг отображения кнопки "применить"
             */
            showApplyBtn: true,
            /**
             * Текст кнопки "применить"
             */
            textApplyBtn: 'применить',
            /**
             * Селектор поиска кнопки "применить"
             */
            selectApplyBtn: 'js-btn-apply',
            /**
             * Флаг отображения кнопки сброса выбранных элементов
             */
            showResetBtn: true,
            /**
             * Текст кнопки "очистить"
             */
            textResetBtn: 'очистить',
            /**
             * Селектор поиска кнопки "сброса"
             */
            selectResetBtn: 'js-btn-reset',
            /**
             * CallBack - выполняется после изменения любого элемента
             */
            callback: function () {
            },

            /**
             * Метод отрисовки блока с базовыми элементами
             */
            drawBaseHtml: function () {
                var self = this;

                var
                html = '<div class="multiselect multiselect-ver2 multiselect-width-auto">';
                    html += '<div class="multiselect-check-value" ' + self.blockClickForOpen + '>';
                        html += '<span ' + self.selectForDefaultName + '>' + self.textForDefaultName + '</span>';
                        html += '<span ' + self.selectForCustomName + ' class="custom-name"></span>';
                        html += '<span class="custom-name-border"></span>';
                    html += '</div>';
                    html += '<div class="multiselect-check-list" ' + self.blockForList + '>';
                        html += '<div class="boxscroll-c objid">';
                            html += '<ul ' + self.selectorForBlockData + '>';
                            html += '</ul>';
                        html += '</div>';
                        if (self.showResetBtn) {
                            html += '<div class="btn-block-reset">' + self.generateResetBtn() + '</div>';
                        }

                        if (self.showApplyBtn) {
                            html += '<div class="btn-block-apply">' + self.generateApplybtn() + '</div>';
                        }

                    html += '</div>';
                html += '</div>';

                self.container.html(html);
            },

            /**
             * Метод генерирует html кнопки "очистить"
             * @returns {string}
             */
            generateResetBtn: function () {
                var self = this;
                var html = '<button class="clean-form" ' + self.selectResetBtn + '> <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve" width="20px" height="20px"> <path d="M16.6,14.4c-1.4,2.2-3.9,3.5-6.6,3.5c-4.3,0-7.9-3.5-7.9-7.9S5.6,2.1,10,2.1c2.1,0,4.1,0.8,5.6,2.3l1.5-1.5 C15.3,1.1,12.8,0,10,0C4.5,0,0,4.5,0,10s4.5,10,10,10c3.5,0,6.5-1.8,8.3-4.5L16.6,14.4z"></path> <polygon points="19,1 19,6.2 13.8,6.2 "></polygon> <circle cx="17.4" cy="15" r="1"></circle> </svg> <span>Очистить</span> </button>';

                return html;
            },

            /**
             * Метод генерирует html кнопки "применить"
             * @returns {string}
             */
            generateApplybtn: function () {
                var self = this;
                var html = '<div class="multiselect-foot"><button class="big-butt" ' + self.selectApplyBtn + '>' + self.textApplyBtn + '</button></div>';;

                return html;
            },

            /**
             * Метод генерирует элементы для выпадающего списка
             */
            generateItemsLine: function () {
                var self = this;

                var block = self.container.find('[' + self.selectorForBlockData + ']');

                if (self.needSelectAll) {
                    var html = '<li><label class="multiselect-checkbox-label"><input class="multiselect-checkbox" name="' + self.nameInput + '" type="' + self.typeInput + '" ' + self.selectAllObject + '><span class="multiselecticon"></span>' + self.selectAllText + '</label></li>';

                    block.append(html);
                }

                $.each(self.data, function (key, value) {
                    var html = '<li><label class="multiselect-checkbox-label"><input class="multiselect-checkbox" name="' + self.nameInput + '" type="' + self.typeInput + '" value="' + value[0] + '"><span class="multiselecticon"></span>' + key + '</label></li>';

                    block.append(html);
                });
            },

            /**
             * Метод иницилизирует клик по кнопке "применить"
             */
            initApplyBtnClick: function () {
                var self = this;

                $('[' + self.selectApplyBtn + ']', self.container).off('click').on('click', function () {
                    self.container.find('[' + self.blockForList + ']').hide();
                });
            },

            /**
             * Метод иницилизирует клик по кнопке "сбросить"
             */
            initResetBtnClick: function () {
                var self = this;

                $('[' + self.selectResetBtn + ']', self.container).off('click').on('click', function () {
                    var cntElChecked = self.container.find('[name="' + self.nameInput + '"]:checked');

                    cntElChecked.prop('checked', false).change();
                });
            },

            /**
             * Метод иницилизации события change на элемент
             */
            initChangeEvent: function () {
                var self = this;

                self.container.find('[name="' + self.nameInput + '"]').off('change').on('change', function (e) {
                    if ($(e.target).attr(self.selectAllObject) != null) {
                        var checked = $(e.target).is(':checked');
                        self.container.find('[name="' + self.nameInput + '"]').each(function () {
                            $(this).prop('checked', checked);
                        });
                    }

                    if (self.containerForChosedElements.length != null && self.containerForChosedElements.length != 0) {
                        self.containerForChosedElements.empty();

                        self.container.find('[name="' + self.nameInput + '"]').each(function () {
                            if ($(this).attr(self.selectAllObject) == null) {
                                self.generateChosedList($(this).val(), $(this).is(':checked'), $(this).closest('li').text().trim(), $(this));
                            }
                        });
                    }


                    if (self.needSelectAll) {
                        var cntEl = self.container.find('[name="' + self.nameInput + '"]:not([' + self.selectAllObject + '])').length;
                        var cntElChecked = self.container.find('[name="' + self.nameInput + '"]:not([' + self.selectAllObject + ']):checked').length;

                        self.container.find('[name="' + self.nameInput + '"][' + self.selectAllObject + ']').prop('checked', cntEl <= cntElChecked);
                    }

                    self.calculateCheckedCheckbox();

                    if (self.callback != null && typeof self.callback == 'function') {
                        self.callback(this);
                    }
                });
            },

            /**
             * Иницилизация события click на элемент для показа/скрытия выпадающего списка
             */
            initClickEvent: function () {
                var self = this;

                var blockBtn = self.container.find('[' + self.blockClickForOpen + ']');
                var blockShow = self.container.find('[' + self.blockForList + ']');
                blockBtn.off('click').on('click', function () {
                    if (blockShow.is(':visible')) {
                        blockShow.hide();
                    } else {
                        blockShow.show();
                        self.container.find('.boxscroll-c').niceScroll({cursorborder: "", cursorcolor: "#00cde3", boxzoom: false});
                    }
                });

                $(document).mouseup(function (e) {
                    if (self.container.find(e.target).length == 0) {
                        blockShow.hide();
                    }
                });
            },

            /**
             * Метод генерирует html для составления блока выбранных элементов в выпадающем списке - помещает в блок containerForChosedElements
             * @param {string} valueElem id
             * @param {string} text текст который будет отображен, как название блока
             * @returns {string} html строка
             */
            generateHtmlForSelectedBlock: function (valueElem, text) {
                var htmlLine = '<div class="item-select-item" data-val="' + valueElem + '">' + text + '</div>';

                return htmlLine;
            },

            /**
             * Метод формирует блок с выбранными элементами из выпадающего списка в отдельный блок
             * @param {string} valueElem значение выбранного жлемента
             * @param {boolean} checkedFlag флаг - отмечен элемент
             * @param {string} text Текст - имя элемента
             * @param {object} el текущий элемент
             */
            generateChosedList: function (valueElem, checkedFlag, text, el) {
                var self = this;

                if (self.containerForChosedElements.length != null && self.containerForChosedElements.length != 0) {
                    if (checkedFlag) {
                        var htmlLine = self.generateHtmlForSelectedBlock(valueElem, text);

                        self.containerForChosedElements.append(htmlLine);

                        self.containerForChosedElements.find('div[data-val="' + valueElem + '"]').on('click', function () {
                            el.prop('checked', false).change();
                        });
                    } else {
                        self.containerForChosedElements.find('div[data-val="' + valueElem + '"]').off('click').remove();
                    }
                }
            },

            /**
             * Метод определяет количество выбранных элементов и выводит количество в selectForCustomName
             */
            calculateCheckedCheckbox: function () {
                var self = this;

                var cntElChecked = self.container.find('[name="' + self.nameInput + '"]:not([' + self.selectAllObject + ']):checked');
                var allElements = self.container.find('[name="' + self.nameInput + '"]:not([' + self.selectAllObject + '])');

                var cntText = '';

                if (cntElChecked.length > 1) {
                    cntText = cntElChecked.length;
                } else {
                    if (cntElChecked[0] != null) {
                        cntText = cntElChecked.closest('li').text();
                    }
                }

                if (cntElChecked.length == allElements.length) {
                    cntText = self.container.find('[' + self.selectAllObject + ']').closest('li').text();
                }

                if (cntText.length > self.maxTextlength) {
                    cntText = cntText.substr(0, self.maxTextlength) + '...';
                }

                self.container.find('[' + self.selectForCustomName + ']').text(cntText);
            },

            /**
             * Метод иницилизации плагина, переопределяет методы, свойства переданные в options
             * @param {object} options объект с настройками
             * @returns {boolean}
             */
            init: function (options) {
                var self = this;

                $.each(self, function () {
                    if (options) {
                        $.extend(self, options);
                    }
                });

                if (self.container == null) {
                    return false;
                }

                self.container.empty();
                if (self.containerForChosedElements.length != null && self.containerForChosedElements.length != 0) {
                    self.containerForChosedElements.empty();
                }

                self.drawBaseHtml();
                self.generateItemsLine();
                self.initChangeEvent();
                self.initClickEvent();

                if (self.showApplyBtn) {
                    self.initApplyBtnClick();
                }

                if (self.showResetBtn) {
                    self.initResetBtnClick();
                }
            },
        };


        options.container = $(this);

        return methods.init(options);
    };
})(jQuery);